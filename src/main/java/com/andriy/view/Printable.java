package com.andriy.view;

import java.io.IOException;

public interface Printable {
    void print() throws IOException;
}
