package com.andriy.view;

import com.andriy.model.Droid;
import com.andriy.model.Ship;

import java.io.*;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.*;

public class MyView {
    Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(){
        menu = new LinkedHashMap<>();

        menu.put("1", "1. Create Ship with Droids. Serialize and deserialize them. Use transient.");
        menu.put("2", "2. Compare reading and writing performance of usual and buffered reader for 200 MB file.");
        menu.put("3", "3. Write your implementation of InputStream with capability of push read data back to the stream.");
        menu.put("4", "4. Write a program that reads a Java source-code file.");
        menu.put("5", "5. Write a program that displays the contents of a specific directory.");
        menu.put("6", "6. Try to create SomeBuffer class, which can be used for read and write data from/to channel.");
        menu.put("7", "7. Write client-server program using NIO.");
        menu.put("Q", "Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressBottom1);
        methodsMenu.put("2", this::pressBottom2);
        methodsMenu.put("3", this::pressBottom3);
        methodsMenu.put("4", this::pressBottom4);
        methodsMenu.put("5", this::pressBottom5);
        methodsMenu.put("6", this::pressBottom6);
        methodsMenu.put("7", this::pressBottom7);
    }

    private void pressBottom1() {
        Droid droid1 =  new Droid(1, "dr1");
        Droid droid2 =  new Droid(2, "dr2");
        Droid droid3 =  new Droid(3, "dr3");
        Droid droid4 =  new Droid(4, "dr4");
        List<Droid> droids1 = new ArrayList<>();
        droids1.add(droid1);
        droids1.add(droid2);
        List<Droid> droids2 = new ArrayList<>();
        droids2.add(droid2);
        droids2.add(droid3);
        List<Droid> droids3 = new ArrayList<>();
        droids3.add(droid3);
        droids3.add(droid4);
        Ship ship1 = new Ship(2,1, "ship1", droids1, 50);
        Ship ship2 = new Ship(3, 2, "ship1", droids2, 100);
        Ship ship3 = new Ship(2, 3, "ship1", droids3, 150);
        ArrayList<Ship> ships = new ArrayList<>();
        ships.add(ship1);
        ships.add(ship2);
        ships.add(ship3);
        System.out.println(Arrays.toString(ships.toArray()));

        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream("Ships.dat"));
            out.writeObject(ships);
            out.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Ship.setCountDroids(3);
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream("Ships.dat"));
            List<Ship> shipsFromFile = (List<Ship>) in.readObject();
            in.close();
            System.out.println(Arrays.toString(shipsFromFile.toArray()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void pressBottom2() throws IOException {
        System.out.println("Test without buffer");
        int count1 = 0;
        InputStream is = new FileInputStream("E:\\Temp\\Book 4.pdf");
        int data = is.read();
        while (data != -1){
            data = is.read();
            count1++;
        }
        is.close();
        System.out.println("count1 = " + count1);
        System.out.println("Test buffered reader");
        int count2 = 0;
        DataInputStream dis = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("E:\\Temp\\Book 4.pdf")));
        try {
            while (true){
                byte b = dis.readByte();
                count2++;
            }
        }catch (EOFException e){

        }

        dis.close();
        System.out.println("count2 = " + count2);

        int count3 = 0;
        System.out.println("Test buffered reader with 5 MB buffered");
        int bufferSize = 5 * 1024 * 1024;
        DataInputStream disC = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("E:\\Temp\\Book 4.pdf"), bufferSize));
        try{
            while (true){
                byte b1 = disC.readByte();
                count3++;
            }
        }catch (EOFException e){

        }
        disC.close();
        System.out.println("count3 = " + count3);
    }

    private void pressBottom3() {

    }

    private void pressBottom4() {

    }

    private void pressBottom5() {
        System.out.println("content of directory");
        File file = new File("E:\\Epam");
        File[] files = file.listFiles();
        if (file.exists()){
            printDirectory(file, "");
//            for (File f : files){
//                if (f.isDirectory()){
//                    System.out.println(f.getName());
//                }
//            }
        }else {
            System.out.println("directory is empty");
        }
    }

    private void printDirectory(File file, String str){
        System.out.println(str + "Directory: " + file.getName());
        str = str + " ";
        File[] files = file.listFiles();
        for (File fil : files) {
            if (fil.isDirectory()) {
                printDirectory(fil, str);
            }else {
                System.out.println(str + "File: " + fil.getName());
            }
        }
    }

    private void pressBottom6() {

    }

    private void pressBottom7() throws IOException {
        Selector selector = Selector.open();
    }

    private void outputMenu() {
        System.out.println("\nMenu");
        for (String st: menu.values()){
            System.out.println(st);
        }
    }

    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point : ");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){

            }
        }while (!keyMenu.equals("Q"));
    }

}
