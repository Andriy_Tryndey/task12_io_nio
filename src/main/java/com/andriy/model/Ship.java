package com.andriy.model;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {
    private static int countDroids = 2;
    private int id;
    private String name;
    private List<Droid> droids;
    private transient int speed;

    public Ship(int  countDroids, int id, String name, List<Droid> droids, int speed) {
        this.countDroids = countDroids;
        this.id = id;
        this.name = name;
        this.droids = droids;
        this.speed = speed;
    }

    public static int getCountDroids() {
        return countDroids;
    }

    public static void setCountDroids(int countDroids) {
        Ship.countDroids = countDroids;
    }

    public int getId() {
        return id;
    }

    public Ship setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Ship setName(String name) {
        this.name = name;
        return this;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public Ship setDroids(List<Droid> droids) {
        this.droids = droids;
        return this;
    }

    public int getSpeed() {
        return speed;
    }

    public Ship setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "countDroids=" + countDroids +
                ",id=" + id +
                ", name='" + name + '\'' +
                ", droids=" + droids +
                ", speed=" + speed +
                '}';
    }
}
