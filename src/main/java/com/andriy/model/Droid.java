package com.andriy.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private int id;
    private String name;

    public Droid(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Droid setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Droid setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
