package com.andriy;

import com.andriy.view.MyView;

import java.io.*;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        new MyView().show();
//        PipedInputStream pipedInputStream = new PipedInputStream();
//        PipedOutputStream pipedOutputStream = new PipedOutputStream();
//
//        pipedInputStream.connect(pipedOutputStream);
//
//        Thread pipeWrite = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 65; i < 91; i++) {
//                    try {
//                        pipedOutputStream.write(i);
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }
//        });
//        Thread pipeReader = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 65; i < 91; i++){
//                    try {
//                        System.out.println(pipedInputStream.read());
//                        Thread.sleep(1000);
//                    } catch (IOException e) {
//
//
//                    } catch (InterruptedException e) {
//
//
//                    }
//                }
//            }
//        });
//
//        pipeWrite.start();
//        pipeReader.start();
//
//        pipeWrite.join();
//        pipeReader.join();
//
//        pipedOutputStream.close();
//        pipedInputStream.close();
    }
}
